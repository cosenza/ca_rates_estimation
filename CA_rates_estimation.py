import numpy as np
import pandas as pd
from scipy.optimize import minimize
import matplotlib.pyplot as plt
import seaborn as sns
import argparse
import multiprocessing as mp
import traceback
import json
from datetime import datetime

ALL_CELLS = []
POP_SIZE = 50000


class Cell():
    def __init__(self, p1, p2, p3, MN=None, SV=None, time=None):
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3
        
        self.time = np.random.choice(20, 1) if time is None else time
        self.alive = True
        self.MN = np.random.choice([True, False], p=[MNI_FREQ, 1-MNI_FREQ]) if MN is None else MN
        if self.MN:
            self.SV = np.random.choice([True, False], p=[SV_FREQ_MNI, 1-SV_FREQ_MNI]) if SV is None else SV
        else:
            self.SV = np.random.choice([True, False], p=[SV_FREQ_NORM, 1-SV_FREQ_NORM]) if SV is None else SV
        
    def run(self):
        if (self.time == 21):
            self.divide()
        self.time += 1
        return
    
    def generate_sv(self, mitosis_type):
        if mitosis_type == 0: # normal
            sv = np.random.choice([True, False], p=[self.p1, 1-self.p1])
        elif mitosis_type == 1: # laggard
            sv = np.random.choice([True, False], p=[self.p2, 1-self.p2])
        elif mitosis_type == 2: # bridge
            sv = np.random.choice([True, False], p=[self.p3, 1-self.p3])
        return sv

    def divide(self):
        def mitosis(kind):
            transitions = {
                0: NOR_TO_MNI, # normal
                1: LAG_TO_MNI, # laggard
                2: BRI_TO_MNI # bridge
            }
            if kind < 3:
                # select destiny
                prob = transitions[kind]

                # sister 1
                sv = self.generate_sv(kind)
                s1 = Cell(self.p1, self.p2, self.p3,
                          MN=np.random.choice([True, False], p=[prob, 1-prob]), 
                          SV=sv, time=0)
                # sister 2
                sv = self.generate_sv(kind)
                s2 = Cell(self.p1, self.p2, self.p3,
                          MN=np.random.choice([True, False], p=[prob, 1-prob]), 
                          SV=sv, time=0)
                
                ALL_CELLS.append(s1)
                ALL_CELLS.append(s2)
            # arrest
            elif kind == 3:
                pass
            
        if not self.MN:
            # pick mitosis type
            # order is normal, laggard, bridge, arrest
            k = np.random.choice([0,1,2,3], 1, p=NORM_TO_MITOSIS)[0]
            mitosis(k)
        elif self.MN:
            k = np.random.choice([0,1,2,3], 1, p=MNI_TO_MITOSIS)[0]
            mitosis(k)
        
        self.alive = False
        return
    

    
def simulation(p1,p2,p3, plot=False, verbose=False):
    
    global ALL_CELLS
    global POP_SIZE
    ALL_CELLS = [Cell(p1, p2, p3) for i in range(50)]
    datapoints = []

    while True:
        [c.run() for c in ALL_CELLS]
        ALL_CELLS = [c for c in ALL_CELLS if c.alive]

        # collect data
        df = pd.DataFrame([(c.alive, c.MN, c.SV) for c in ALL_CELLS], columns=['alive', 'mn', 'sv'])
        if df[df['alive']].shape[0] == 0:
            #print('extinct')
            break
        p = df[df['alive']].groupby('mn')['sv'].value_counts()
        datapoints.append(p)

        print(len(ALL_CELLS), end='\r', flush=True)
        if len(ALL_CELLS) > POP_SIZE:
            break
            
    df = pd.concat(datapoints,axis=1)
    # SV frequency
    norm_freq = df.loc[(False, True), :] / (df.loc[(False, True), :] + df.loc[(False, False), :]) * 100
    mni_freq = df.loc[(True, True), :] / (df.loc[   (True, True), :] + df.loc[(True, False), :]) * 100

    # MNI frequency
    mni_pheno_freq = (df.loc[   (True, True), :] + df.loc[(True, False), :]) / ((df.loc[   (True, True), :] + df.loc[(True, False), :]) + (df.loc[(False, True), :] + df.loc[(False, False), :])) * 100

    if plot:

        fig, ax = plt.subplots(figsize=(8,8), nrows=2)
        sns.lineplot(x=np.arange(norm_freq.shape[0]), y=norm_freq.values, label='normal', ax=ax[0])
        sns.lineplot(x=np.arange(mni_freq.shape[0]), y=mni_freq.values, label='micronucleated', ax=ax[0])
        sns.despine()
        ax[0].set_ylabel('frequency (%)')
        ax[0].legend()

        sns.lineplot(x=np.arange(mni_pheno_freq.shape[0]), y=mni_pheno_freq.values, label='micronuclei frequency', ax=ax[1])
       
    
    o1 = norm_freq.values[-1] - (SV_FREQ_NORM*100)
    o2 = mni_freq.values[-1] - (SV_FREQ_MNI*100)
    
    if verbose:
        print(p1, p2, p3, (o1*o1) + (o2*o2))
    return (o1*o1) + (o2*o2)


def simulation_optimization(x0):
    return simulation(*x0)

def optimization_round(args):
    try:
        p1, p2, p3, q = args
        x0 = [p1, p2, p3]
        out = minimize(simulation_optimization, x0=x0, bounds=[(0,1), (0,1), (0,1)], method='Powell', tol=0.0001)
        line = str(out.x) + '\t' + str(out.fun) + '\n'
        print('round done:', out.x, out.fun)
        q.put(line)
    except Exception as e:
        print(e)
    return

def listener(q, SAVEPATH):
    while 1:
        m = q.get()
        with open(SAVEPATH, 'a') as f:
            if m == 'done':
                f.write(m)
                break
            f.write(m)

    return
            
if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    
    ap.add_argument('-i', '--initial_params', required=True, help='path to table with initial parameters')
    ap.add_argument('-m', '--model', required=True, help='model parameters, choose from: WT or TP53KO')
    ap.add_argument('-p', '--pop_size', required=False, help='minimum population size')
    ap.add_argument('-sv', '--sv_freq_norm', required=False, help='override SV freq of normal cells and correct micronucleated by subtracting it')
    ap.add_argument('-o', '--output', required=False, help='output filepath (path and filename)')

    args = vars(ap.parse_args())
    
    if args['pop_size'] is not None:
        POP_SIZE = int(args['pop_size'])
    
    
    if args['model'] == 'WT':
        # MCF10A wild-type
        MNI_FREQ = 0.058
        SV_FREQ_NORM = 0.182
        SV_FREQ_MNI = 0.549
        MNI_TO_MITOSIS = [.667, .172, .032, 1-(.667 + .172 + .032)]
        NORM_TO_MITOSIS = [.891, .046, .045, 1-(.891 + .046 + .045)]
        LAG_TO_MNI = .323  
        BRI_TO_MNI = .172
        NOR_TO_MNI = .029
    elif args['model'] == 'TP53KO':
        # MCF10A p53-KO
        MNI_FREQ = 0.322
        SV_FREQ_NORM = 0.417
        SV_FREQ_MNI = 0.932
        # order is normal, laggard, bridge, arrest
        MNI_TO_MITOSIS = [.294, .134, .491, 1-(.294 + .134 + .491)]
        NORM_TO_MITOSIS = [.634, .049, .292, 1-(.634 + .049 + .292)]
        LAG_TO_MNI = .733  
        BRI_TO_MNI = .697
        NOR_TO_MNI = .097
    else:
        raise ValueError('invalid model, choose "WT" or "TP53KO"')


    OVERRIDE = False    
    if args['sv_freq_norm'] is not None:
        OVERRIDE = True
        SV_FREQ_NORM_OVERRIDE = float(args['sv_freq_norm'])
    else:
        SV_FREQ_NORM_OVERRIDE = SV_FREQ_NORM

    INITPATH = args['initial_params']

    now = datetime.now()
    dt_string = now.strftime("%Y-%m-%d_%H-%M")

    if OVERRIDE:
        SV_FREQ_MNI = SV_FREQ_MNI - (SV_FREQ_NORM - SV_FREQ_NORM_OVERRIDE)
        SV_FREQ_NORM = SV_FREQ_NORM_OVERRIDE

    params = dict(
        MODEL = args['model'],
        POP_SIZE = POP_SIZE,
        MNI_FREQ = MNI_FREQ,
        SV_FREQ_NORM = SV_FREQ_NORM, 
        SV_FREQ_MNI = SV_FREQ_MNI,
        MNI_TO_MITOSIS = MNI_TO_MITOSIS,
        NORM_TO_MITOSIS = NORM_TO_MITOSIS,
        LAG_TO_MNI = LAG_TO_MNI,  
        BRI_TO_MNI = BRI_TO_MNI,
        NOR_TO_MNI = NOR_TO_MNI,
        INIT_PARAMS = INITPATH
    )

    SAVEPATH = '{}_sv_rates_estimates_{}_pop{}_svnorm{:.2f}.csv'.format(dt_string, args['model'], POP_SIZE, SV_FREQ_NORM_OVERRIDE) if args['output'] is None else args['output']
    SAVEPARAMS = '{}_sv_rates_parameters_{}_pop{}_svnorm{:.2f}.json'.format(dt_string, args['model'], POP_SIZE, SV_FREQ_NORM_OVERRIDE) if args['output'] is None else args['output']
    
    with open(SAVEPARAMS, 'w') as fp:
        json.dump(params, fp)

    df = pd.read_csv(INITPATH, index_col=0)
    x0_list = list(list(a) for a in df.values)

    pool = mp.Pool(46)
    manager = mp.Manager()
    q = manager.Queue()

    print('start')
    try:        
        writer = mp.Process(target=listener, args=(q, SAVEPATH))
        writer.daemon = True
        writer.start()
    
        # add queue object to argument
        print(len(x0_list))
        print(x0_list[0])
        for i in x0_list:
            i.append(q)
        pool.map(optimization_round, x0_list)

    except Exception as e:
        print(traceback.format_exc())
        print(e)
    
    q.put('done')
    print('Q: ', q.qsize())
    pool.close()
    pool.join()
    print('done')