# CA_rates_estimation

## How-to-use
Clone the repository  
`git clone https://git.embl.de/cosenza/ca_rates_estimation.git && cd ca_rates_estimation`  

Install pip requirements  
`pip install -r requirements`  

To run the CA_rates_estimation.py script, simply have a look at the arguments like this:  
`python CA_rates_estimation.py -h`  

## Contributors

- Marco Raffaele Cosenza (maintainer and developer)

## Contact

Please, contact Marco Raffaele Cosenza (marco.cosenza(at)embl.de) in case you have any questions.
